import java.util.*

fun main(args: Array<String>){

    var bottiglie:Array<Bottiglia?> = arrayOfNulls(3)

    bottiglie[0] = Bottiglia("975AG", 1800, Tipi.Rosso, 20.50 )
    bottiglie[1] = Bottiglia("923LO", 1900, Tipi.Bianco, 15.20)
    bottiglie[2] = Bottiglia("168OK", 2010, Tipi.Rosato, 18.60)

    Locale.setDefault(Locale.ITALY)

    val input = Scanner(System.`in`)
    var code = input.next()

    var a = 0

    bottiglie.indices
            .filter { bottiglie[it]?.getCodice().equals(code) }
            .forEach { a = bottiglie[it]?.getAnno()!! }

    println(bottiglie[a].toString())

    var media = bottiglie.indices.sumByDouble { bottiglie[it]?.getPrezzo()!! }

    media/=bottiglie.size

    println("Il prezzo medio delle bottiglie è " + media)

}