import kotlin.system.exitProcess

class Bottiglia(private var codice:String, private var anno: Int, private var tipo: Tipi, private var prezzo: Double) {

    fun setCodice(c: String) {
        codice = c
    }

    fun setAnno(a: Int) {
        anno = a
    }

    fun setTipo(t: Tipi) {
        tipo = t
    }

    fun setPrezzo(p: Double) {
        prezzo = p
    }

    fun getCodice(): String = codice

    fun getAnno(): Int = anno

    fun getTipo(): Tipi = tipo

    fun getPrezzo(): Double = prezzo

    fun getAnnoByCodice(c:String):Int{
        return if(c==codice)
            anno
        else -1
    }

    fun checkPrezzo(b:Bottiglia) =
            when {
                this.prezzo>b.getPrezzo() -> println("La bottiglia di " + this.tipo.name + " ha un prezzo maggiore rispetto alla bottiglia di " + b.getTipo().name)
                this.prezzo<b.getPrezzo() -> println("La bottiglia di " + this.tipo.name + "ha un prezzo minore rispetto alla bottiglia di " + b.getTipo().name)
                this.prezzo==b.getPrezzo() -> println("Le due bottiglie hanno lo stesso prezzo")
                else -> {
                    println("Qualcosa è andato storto :/")
                    exitProcess(-2)
                }
            }

    override fun toString():String = codice +" " + tipo.name + " " + prezzo + " " + anno
}